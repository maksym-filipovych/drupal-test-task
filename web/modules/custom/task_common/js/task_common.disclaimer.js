(function ($, Drupal, drupalSettings) {

  let isInit = false;

  function redirectToFrontPage() {
    window.location.href = drupalSettings.path.baseUrl;
  }

  function createOverlayDisclaimer(data) {
    let disclaimers = localStorage.getItem('disclaimers_mark');
    disclaimers = disclaimers ? JSON.parse(disclaimers) : {};
    if (disclaimers[data.node_id]) {
      return;
    }

    let overlayPopup = null;

    let html = '';
    html += '<div id="task-common-disclaimer">';
    html += ' <div id="overlay-background">';
    html += ' </div>';
    html += ' <div class="overlay-container">';
    html += '   <div class="overlay-header row">';
    html += '     <div class="col-sm-11">';
    html +=         Drupal.t('Important information');
    html += '     </div>';
    html += '     <div class="col-sm-1 text-right">';
    html += '       <span class="overlay-close glyphicon glyphicon-remove"></span>';
    html += '     </div>';
    html += '   </div>';
    html += '   <div class="overlay-content row">';
    html += '     <div class="col-sm-12">';
    html +=         data.text;
    html += '     </div>';
    html += '   </div>';
    html += '   <div class="overlay-footer row">';
    html += '     <div class="col-sm-9">';
    html += '     </div>';
    html += '     <div class="col-sm-3 text-right">';
    html += '       <button class="accept btn btn-success">';
    html += '         <span class="glyphicon glyphicon-ok"></span>'+ Drupal.t('Accept');
    html += '       </button>';
    html += '       <button class="decline btn btn-danger">';
    html += '         <span class="glyphicon glyphicon-remove"></span>'+ Drupal.t('Decline');
    html += '       </button>';
    html += '     </div>';
    html += '   </div>';
    html += ' </div>';
    html += '</div>';

    overlayPopup = $(html).appendTo(document.body);

    $('#task-common-disclaimer').show();

    $('#task-common-disclaimer .overlay-footer button.accept').bind('click', function () {
      $(overlayPopup).remove();
      disclaimers[data.node_id] = 1;
      localStorage.setItem('disclaimers_mark', JSON.stringify(disclaimers));
    });
    $('#task-common-disclaimer .overlay-footer button.decline').bind('click', redirectToFrontPage);
    $('#task-common-disclaimer .overlay-header .overlay-close').bind('click', redirectToFrontPage);
  }

  Drupal.behaviors.TaskCommonDisclaimerBehavior = {
    attach: function (context, settings) {

      if (isInit) {
        return;
      }
      if (drupalSettings.task_common.disclaimer) {
        createOverlayDisclaimer(drupalSettings.task_common.disclaimer);
        isInit = !isInit;
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
