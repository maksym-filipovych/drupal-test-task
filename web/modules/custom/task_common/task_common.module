<?php

/**
 * @file
 *
 * Contains common functionality
 */

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\NodeInterface;
use Drupal\user\Entity\User;

/**
 * Implements hook_entity_view().
 *
 * @param array $build
 * @param EntityInterface $entity
 * @param EntityViewDisplayInterface $display
 * @param string $view_mode
 *
 * @return null
 */
function task_common_entity_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  $current_language = Drupal::languageManager()->getCurrentLanguage();
  $current_user = User::load(\Drupal::currentUser()->id());
  $current_route_name = \Drupal::routeMatch()->getRouteName();
  if (($current_route_name === 'entity.node.canonical') && !$current_user->hasRole('administrator') && ($entity instanceof NodeInterface)) {
    if ($entity->hasField('field_disclaimer') && ($disclaimer = $entity->get('field_disclaimer')->entity)) {
      if ($disclaimer->hasTranslation($current_language->getId())) {
        $disclaimer = $disclaimer->getTranslation($current_language->getId());
      }
      $build['#attached']['library'][] = 'task_common/task_common.disclaimer';
      $build['#attached']['drupalSettings']['task_common']['disclaimer'] = [
        'node_id' => $entity->id(),
        'text' => $disclaimer->get('field_description')->value,
      ];
    }
  }
}
